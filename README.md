This is an implementation to a solution to http://geeks.redmart.com/2015/01/07/skiing-in-singapore-a-coding-diversion/ 

The solution is implemented in both Java and Swift2. 

To compile in Java:

```
> cd SkiingSingapore/java/SkiingSingapore
> javac -d ./bin -cp ./src src/com/DoNation/SkiingSingapore/SkiingSingapore.java
> cd bin
> java com.DoNation.SkiingSingapore.SkiingSingapore ../../map_large.txt
```

To compile in Swift2:

```
> cd SkiingSingapore/swift2
> swiftc -o SkiingSingapore ./*.swift
>  ./SkiingSingapore ../map_large.text
```
