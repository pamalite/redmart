package com.DoNation.SkiingSingapore;

import com.DoNation.SkiingSingapore.MapAnalyzer.PossiblePath;

public class SkiingSingapore {
	public static void showUsage() {
		System.out.println("Usage: java " + SkiingSingapore.class.getName() + " <path_to_map.txt_file>");
	}
	
	public static void main(String [] args) {
		if (args.length != 1) {
			SkiingSingapore.showUsage();
			return;
		}
		
		// initialize Map
		Map map = new Map(args[0]);
		map.initialize();
		
		System.out.println("Columns: " + Integer.toString(map.getNumColumns()) + ", Rows: " + Integer.toString(map.getNumRows()));
				
		// analyze map
		MapAnalyzer mapAnalyzer = new MapAnalyzer(map);
		mapAnalyzer.doPreAnalysis();
		
		System.out.println("Possible Highest Points: " + mapAnalyzer.getHighestPoints().toString());
		System.out.println("Highest Point Value: " + Integer.toString(mapAnalyzer.getHighestPointValue()));
		System.out.println("Possible Lowest Points: " + mapAnalyzer.getLowestPoints().toString());
		System.out.println("Lowest Point Value: " + Integer.toString(mapAnalyzer.getLowestPointValue()));
		
		mapAnalyzer.doPathAnalysis();
		
		PossiblePath bestPath = mapAnalyzer.getBestPath();
		System.out.println("Best Path: \n* " + bestPath + " *");
		
//		System.out.println("All Possible Paths:");
//		int limit = 80;
//		for (PossiblePath possiblePath : mapAnalyzer.getPossiblePaths()) {
//			System.out.println("- " + possiblePath.toString());
//			
//			if (limit != 0) {
//				//limit--;
//			} else {
//				break;
//			}
//		}
		
		return;
	}
}
