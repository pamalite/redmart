package com.DoNation.SkiingSingapore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.DoNation.SkiingSingapore.Map.Point;

public class MapAnalyzer {
	public class PossiblePath implements Cloneable, Comparable<PossiblePath> {
		private List<Map.Point> points;
		
		public PossiblePath() {
			this.points = new ArrayList<Map.Point>();
		}
		
		public PossiblePath(List<Map.Point> _points) {
			this();
			if (_points != null) {
				this.points = _points;
			}
		}
		
		public List<Map.Point> getPoints() {
			return this.points;
		}
		
		public void addPoint(Map.Point _point) {
			this.points.add(_point);
		}
		
		public void removeLastPoint() {
			this.points.remove(this.points.size()-1);
		}
		
		public int getLength() {
			return this.points.size();
		}
		
		public int getDrop() {
			return (this.points.get(0).getValue() - this.points.get(this.points.size()-1).getValue());
		}
		
		public String toString() {
			String path = "";
			int count = 0;
			for (Map.Point point : this.points) {
				path = path + point.toString();
				
				if (count < this.points.size()-1) {
					path = path + " -> ";
				}
				
				count++;
			}
			
			return "Length: " + Integer.toString(this.points.size()) + ", Drop: " + Integer.toString(this.getDrop()) + ", Path: [" + path + "]";
		}
		
		public Object clone() {
			PossiblePath clonedPath = null;
			try {
				clonedPath = (PossiblePath) super.clone();
			} catch (CloneNotSupportedException _ex) {
				throw new InternalError(_ex.toString());
			}
			clonedPath.points = new ArrayList<Map.Point>(this.points);
			return clonedPath;
		}

		@Override
		public int compareTo(PossiblePath _anotherPath) {
			// order by drop
			return -(this.getLength() - _anotherPath.getLength());
		}
	}
	
	private Map map;
	
	private List<Map.Point> highestPoints;
	private List<Map.Point> lowestPoints;
	private List<PossiblePath> possiblePaths;
	
	private int highestPointValue;
	private int lowestPointValue;
	
	public MapAnalyzer(Map _map) {
		this.map = _map;
		
		this.initialize();
	}
	
	private void initialize() {
		this.highestPoints = null;
		this.lowestPoints = null;
		this.possiblePaths = null;
		
		this.highestPointValue = Integer.MAX_VALUE;
		this.lowestPointValue = Integer.MIN_VALUE;
	}
	
	private int getGlobalMaximumFrom(List<Map.Point> _points) {
		int globalMaxValue = Integer.MIN_VALUE;
		for (Map.Point point : _points) {
			if (point.getValue() > globalMaxValue) {
				globalMaxValue = point.getValue();
			}
		}
		
		return globalMaxValue;
	}
	
	private int getGlobalMinimumFrom(List<Map.Point> _points) {
		int globalMinValue = Integer.MAX_VALUE;
		for (Map.Point point : _points) {
			if (point.getValue() < globalMinValue) {
				globalMinValue = point.getValue();
			}
		}
		
		return globalMinValue;
	}
	
	private void findHighestPoints() {
		for (int y = 0; y < map.getNumRows(); y++) {
			int globalMaxValue = this.getGlobalMaximumFrom(map.getRow(y));
			
			List<Map.Point> considerations = new ArrayList<Map.Point>();
			for (int x = 0; x < map.getNumColumns(); x++) {
				Map.Point point = map.getPoint(x, y);
				
				if (point.getValue() == globalMaxValue) {
					considerations.add(point);
				}
			}
			
			// add to list
			this.highestPoints.addAll(considerations);
		}
	}
	
	private void cleanHighestPoints() {
		if (this.highestPoints == null || this.highestPoints.isEmpty()) {
			return;
		}
		
		int globalMaxValue = this.getGlobalMaximumFrom(this.highestPoints);
		
		List<Map.Point> cleaned = new ArrayList<Map.Point>();
		for (Map.Point highest : this.highestPoints) {
			if (highest.getValue() == globalMaxValue) {
				cleaned.add(highest);
			}
		}
		
		this.highestPoints.clear();
		this.highestPoints = cleaned;
		
		this.highestPointValue = globalMaxValue;
	}
	
	private void findLowestPoints() {
		for (int y = 0; y < map.getNumRows(); y++) {
			int globalMinValue = this.getGlobalMinimumFrom(map.getRow(y));
			
			List<Map.Point> considerations = new ArrayList<Map.Point>();
			for (int x = 0; x < map.getNumColumns(); x++) {
				Map.Point point = map.getPoint(x, y);
				
				if (point.getValue() == globalMinValue) {
					considerations.add(point);
				}
			}
			
			// add to list
			this.lowestPoints.addAll(considerations);
		}
	}
	
	private void cleanLowestPoints() {
		if (this.lowestPoints == null || this.lowestPoints.isEmpty()) {
			return;
		}
		
		int globalMinValue = this.getGlobalMinimumFrom(this.lowestPoints);
		
		List<Map.Point> cleaned = new ArrayList<Map.Point>();
		for (Map.Point lowest : this.lowestPoints) {
			if (lowest.getValue() == globalMinValue) {
				cleaned.add(lowest);
			}
		}
		
		this.lowestPoints.clear();
		this.lowestPoints = cleaned;
		
		this.lowestPointValue = globalMinValue;
	}
	
	public void doPreAnalysis() {
		if (this.map == null) {
			return;
		}
		
		// clear all
		this.highestPoints = new ArrayList<Map.Point>();
		this.lowestPoints = new ArrayList<Map.Point>();
		this.possiblePaths = new ArrayList<PossiblePath>();
		
		// scan for highest and lowest points
		this.findHighestPoints();
		this.cleanHighestPoints();
		
		this.findLowestPoints();
		this.cleanLowestPoints();
	}
	
	private List<Map.Point> getNextDescendingPoints(Map.Point _currentPoint) {
		if (_currentPoint == null) {
			return null;
		}
		
		List<Map.Point> points = new ArrayList<Map.Point>();
		int x, y = 0;
		
		// north
		x = _currentPoint.getPosX();
		y = _currentPoint.getPosY() - 1;
		if (y >= 0) {
			// make sure do not return out of bounds on top edge
			Map.Point point = this.map.getPoint(x, y);
			if (point.getValue() < _currentPoint.getValue()) {
				points.add(point);
			}
		}
		
		// east
		x = _currentPoint.getPosX() + 1;
		y = _currentPoint.getPosY();
		if (x <= this.map.getNumColumns()-1) {
			Map.Point point = this.map.getPoint(x, y);
			if (point.getValue() < _currentPoint.getValue()) {
				points.add(point);
			}
		}
		
		// south
		x = _currentPoint.getPosX();
		y = _currentPoint.getPosY() + 1;
		if (y <= this.map.getNumRows()-1) {
			Map.Point point = this.map.getPoint(x, y);
			if (point.getValue() < _currentPoint.getValue()) {
				points.add(point);
			}
		}
		
		// west
		x = _currentPoint.getPosX() - 1;
		y = _currentPoint.getPosY();
		if (x >= 0) {
			Map.Point point = this.map.getPoint(x, y);
			if (point.getValue() < _currentPoint.getValue()) {
				points.add(point);
			}
		}
		
		return points;
	}
	
	private void buildPathWith(Map.Point _currentPoint, PossiblePath _currentPath) {
		// add currentPoint to existingPath
		_currentPath.addPoint(_currentPoint);
		
		List<Map.Point> adjacentPoints = this.getNextDescendingPoints(_currentPoint);
		if (adjacentPoints == null || adjacentPoints.isEmpty()) {
			PossiblePath pathCopy = (PossiblePath) _currentPath.clone();
			// System.out.println(pathCopy.toString());
			this.possiblePaths.add(pathCopy);
			return;
		}
		
		// run through each adjacentPoints
		for (Map.Point adjacentPoint : adjacentPoints) {
			// build the path with the current adjacentPoint
			this.buildPathWith(adjacentPoint, _currentPath);
			_currentPath.removeLastPoint();
		}
	}
	
	public void doPathAnalysis() {
		if (this.map == null ||
			(this.highestPoints == null || this.highestPoints.isEmpty()) ||
			(this.lowestPoints == null || this.lowestPoints.isEmpty())) {
			return;
		}
		
		// clear all
		this.possiblePaths = new ArrayList<MapAnalyzer.PossiblePath>();
		
		// scan for all possible paths from all possible highest
		for (Map.Point point : this.highestPoints) {
			this.buildPathWith(point, new PossiblePath());
		}
	}
	
	public PossiblePath getBestPath() {
		if (this.possiblePaths == null || this.possiblePaths.isEmpty()) {
			return null;
		}
		
		// sort the paths by length value in descending order
		Collections.sort(this.possiblePaths);
		
		// get the longest path
		PossiblePath bestPath = null;
		
		int dropValue = Integer.MIN_VALUE;
		int currentLength = Integer.MIN_VALUE;
		for (PossiblePath path : this.possiblePaths) {
			if (currentLength == Integer.MIN_VALUE) {
				dropValue = path.getDrop();
				currentLength = path.getLength();
				bestPath = path;
				continue;
			} 
			
			if (path.getLength() < currentLength) {
				// done. i need longest + steepest!
				break;
			}
			
			if (path.getDrop() > dropValue) {
				dropValue = path.getDrop();
				bestPath = path;
			}
		}
		
		return bestPath;
	}
	public List<Point> getHighestPoints() {
		return this.highestPoints;
	}
	
	public List<Point> getLowestPoints() {
		return this.lowestPoints;
	}
	
	public List<PossiblePath> getPossiblePaths() {
		return this.possiblePaths;
	}
	
	public int getHighestPointValue() {
		return this.highestPointValue;
	}
	
	public int getLowestPointValue() {
		return this.lowestPointValue;
	}
}
