package com.DoNation.SkiingSingapore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Map {
	public class Point implements Comparable<Point>{
		private int posX;
		private int posY;
		private int value;
		
		public Point(int _posX, int _posY, int _value) {
			this.posX = _posX;
			this.posY = _posY;
			this.value = _value;
		}
		
		public int getPosX() {
			return this.posX;
		}
		
		public int getPosY() {
			return this.posY;
		}
		
		public int getValue() {
			return this.value;
		}
		
		public String toString() {
			return "(" + Integer.toString(this.posX) + "," + Integer.toString(this.posY) + "," + Integer.toString(this.value) + ")";
		}

		@Override
		public int compareTo(Point _anotherPoint) {
			if (this.posX < _anotherPoint.getPosX()) {
				return -1;
			} else if (this.posX > _anotherPoint.getPosX()) {
				return 1;
			}
			return 0;
		}
	}
	
	private String fileURL;
	
	private List<List<Point>> points;
	private int numColumns;
	private int numRows;
	
	public Map(String _fileURL) {
		this.fileURL = (_fileURL != null && !_fileURL.isEmpty()) ? _fileURL : null;
	}
	
	public void initialize() {
		if (this.fileURL == null) {
			return;
		}
		
		// read from file
		this.points = new ArrayList<List<Point>>();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(this.fileURL));
			
			// data for double-checking
			this.numColumns = 0;
			this.numRows = 0;
			
			int rowCount = 0;
			String aLine;
			while((aLine = reader.readLine()) != null) {
				String [] values = aLine.split(" ");
				
				// first line has only 2 values numColumns & numRows
				if (values.length == 2) {
					this.numColumns = Integer.valueOf(values[0]);
					this.numRows = Integer.valueOf(values[1]);
					continue;
				}
				
				// subsequent lines
				// check number of columns
				if (values.length != this.numColumns) {
					// ignore faulty line
					continue;
				}
				
				// extract all points
				int columnCount = 0;
				List<Point> row = new ArrayList<Point>();
				for (String value: values) {
					Point point = new Point(columnCount, rowCount, Integer.valueOf(value));
					row.add(point);
					columnCount++;
				}
				this.points.add(row);
				
				rowCount++;
			}
			
			reader.close();
			
			// check line count
			if (rowCount != this.numRows) {
				System.out.println("Invalid Map.");
				return;
			}
		} catch(IOException _ex) {
			System.out.println(_ex.getMessage() + "\n" + _ex.getStackTrace().toString());
		}
	}
	
	public List<List<Point>> getAllPoints() {
		return this.points;
	}
	
	public Point getPoint(int _posX, int _posY) {
		return this.points.get(_posY).get(_posX);
	}
	
	public List<Point> getRow(int _row) {
		return this.points.get(_row);
	}
	
	public int getNumColumns() {
		return this.numColumns;
	}
	
	public int getNumRows() {
		return this.numRows;
	}
}
