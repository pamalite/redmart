//
//  Map.swift
//  
//
//  Created by Ken S'ng Wong on 30/04/2016.
//
//

import Foundation

class Map {
    private(set) var numberOfColumns: Int?
    private(set) var numberOfRows: Int?
    private(set) var points: [[Point]]?
    
    init(mapFile: String?) {
        if mapFile == nil {
            return
        }
        
        // open and read the file
        do {
            let rawMap: String = try String(contentsOfFile: mapFile!, encoding: NSUTF8StringEncoding)
            
            if rawMap.isEmpty {
                throw NSError(domain: "Map Read Error", code: -1, userInfo: [NSLocalizedDescriptionKey : "Empty file read."])
            }
            
            let lines: [String] = rawMap.componentsSeparatedByString("\n")
            
            var isMetaDone: Bool = false
            var lineCount: Int = 0
            for line: String in lines {
                // skip empty line
                if line.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).isEmpty {
                    continue
                }
                
                if !isMetaDone {
                    // meta data
                    let items: [String] = line.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).componentsSeparatedByString(" ")
                    if items.count != 2 {
                        throw NSError(domain: "Map Read Error", code: -2, userInfo: [NSLocalizedDescriptionKey : "Invalid row and column line."])
                    }
                    
                    self.numberOfColumns = Int(items[0])
                    self.numberOfRows = Int(items[1])
                    self.points = [[Point]]()
                    
                    isMetaDone = true
                    continue
                }
                
                // points
                let items: [String] = line.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).componentsSeparatedByString(" ")
                if items.count < self.numberOfColumns {
                    throw NSError(domain: "Map Read Error", code: -3, userInfo: [NSLocalizedDescriptionKey : "Invalid elevation line."])
                }
                
                var row: [Point] = [Point]()
                var itemCount: Int = 0
                for item: String in items {
                    let point: Point = Point(x: itemCount, y: lineCount, elevation: Int(item)!)
                    row.append(point)
                    
                    itemCount += 1
                }
                self.points?.append(row)
                
                lineCount += 1
            }
            
            // check
            if self.numberOfRows == self.points!.count {
                for row: [Point] in self.points! {
                    if row.count != self.numberOfColumns {
                        throw NSError(domain: "Map Parse Error", code: -1, userInfo: [NSLocalizedDescriptionKey : "Number of columns mismatch."])
                    }
                }
            } else {
                throw NSError(domain: "Map Parse Error", code: -2, userInfo: [NSLocalizedDescriptionKey : "Number of rows mismatch."])
            }
        } catch let err as NSError {
            NSLog(err.localizedDescription)
            self.reset()
        }
    }
    
    private func reset() {
        self.numberOfColumns = nil
        self.numberOfRows = nil
        self.points = nil
    }

    func description() -> String {
        var linesStr: String = ""
        for row: [Point] in self.points! {
            var colCount: Int = 0
            var lineStr: String = ""
            for col: Point in row {
                lineStr = lineStr + col.description()
                
                if colCount < row.count-1 {
                    lineStr = lineStr + ", "
                }
                colCount += 1
            }
            linesStr = linesStr + "\n" + lineStr
        }
        
        return linesStr
    }
    
    func pointAt(x: Int, y: Int) -> Point? {
        let point: Point = self.points![y][x]
        return point
    }
}