//
//  main.swift
//  SkiingSingapore
//
//  Created by Ken S'ng Wong on 30/04/2016.
//  Copyright © 2016 Ken S'ng Wong. All rights reserved.
//

import Foundation

func stringify(points: [Point]) -> String {
    var str: String = ""
    
    var count: Int = 0
    for point: Point in points {
        str = str + point.description()
        
        if count < points.count-1 {
            str = str + ", "
        }
        count += 1
    }
    
    return "[\(str)]"
}

func main(mapFile: String?) {
    // read the map
    let map: Map = Map(mapFile: mapFile!)
    print(String(format: "Columns: %d, Rows: %d", map.numberOfColumns!, map.numberOfRows!))
    
    // pre-analysis
    let mapAnalyzer: MapAnalyzer = MapAnalyzer(map: map)
    mapAnalyzer.doPreAnalysis()
    
    print("Possible Highest Points: \(stringify(mapAnalyzer.highestPoints!))")
    print(String(format: "Highest Point Value: %d", mapAnalyzer.highestPointValue!))
    print("Possible Lowest Points: \(stringify(mapAnalyzer.lowestPoints!))")
    print(String(format: "Lowest Point Value: %d", mapAnalyzer.lowestPointValue!))
    
    mapAnalyzer.doPathAnalysis()
    
    let bestPath: PossiblePath? = mapAnalyzer.bestPath()
    print(String(format: "Best Path: \n* %@ *", bestPath!.toString()))
    
}

// begin
let args: [String] = Process.arguments

if args.count != 2 {
    print("Usage: " + args[0] + " <map_file.txt>")
    exit(0)
}

// read the cli args

//let fileManager = NSFileManager.defaultManager()
//let cwd = fileManager.currentDirectoryPath
//let mapFile: String = cwd + "/map_large.txt"

let mapFile: String = args[1]
main(mapFile)

exit(0)
