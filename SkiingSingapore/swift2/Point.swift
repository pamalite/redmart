//
//  Point.swift
//  
//
//  Created by Ken S'ng Wong on 30/04/2016.
//
//

import Foundation

class Point {
    // properties
    let x: Int?
    let y: Int?
    let value: Int?
    
    init(x: Int, y: Int, elevation: Int) {
        self.x = x
        self.y = y
        self.value = elevation
    }
    
    func description() -> String {
        return String(format: "(%d,%d,%d)", self.x!, self.y!, self.value!)
    }
}