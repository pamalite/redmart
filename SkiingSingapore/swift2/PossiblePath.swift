//
//  PossiblePath.swift
//  SkiingSingapore
//
//  Created by Ken S'ng Wong on 01/05/2016.
//  Copyright © 2016 Ken S'ng Wong. All rights reserved.
//

import Foundation

class PossiblePath: NSObject, NSCopying {
    private(set) var points: [Point]?
    
    override init() {
        self.points = [Point]()
    }
    
    init(points: [Point]) {
        self.points = points
    }
    
    func length() -> Int {
        return self.points!.count
    }
    
    func drop() -> Int {
        return (self.points!.first!.value! - self.points!.last!.value!)
    }
    
    func add(point: Point) {
        self.points!.append(point)
    }
    
    func removeLast() {
        self.points!.removeLast()
    }
    
    func toString() -> String {
        var path: String = ""
        var count: Int = 0
        for point: Point in self.points! {
            path = path + point.description()
            
            if count < self.points!.count-1 {
                path = path + " -> "
            }
            count += 1
        }
        
        return String(format: "Length: %d, Drop: %d, Path: [%@]", self.length(), self.drop(), path)
    }
    
    func copyWithZone(zone: NSZone) -> AnyObject {
        let copy = PossiblePath(points: self.points!)
        return copy
    }
}