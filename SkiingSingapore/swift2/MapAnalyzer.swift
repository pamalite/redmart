//
//  MapAnalyzer.swift
//  SkiingSingapore
//
//  Created by Ken S'ng Wong on 01/05/2016.
//  Copyright © 2016 Ken S'ng Wong. All rights reserved.
//

import Foundation

class MapAnalyzer {
    private let map: Map?
    
    private(set) var highestPoints: [Point]?
    private(set) var lowestPoints: [Point]?
    private(set) var possiblePaths: [PossiblePath]?
    
    private(set) var highestPointValue: Int?
    private(set) var lowestPointValue: Int?
    
    init(map: Map) {
        self.map = map
    }
    
    private func globalMaximumFrom(points: [Point]) -> Int {
        var globalMaxValue: Int = Int.min
        for point: Point in points {
            if point.value! > globalMaxValue {
                globalMaxValue = point.value!
            }
        }
        return globalMaxValue
    }
    
    private func globalMinimumFrom(points: [Point]) -> Int {
        var globalMinValue: Int = Int.max
        for point: Point in points {
            if point.value! < globalMinValue {
                globalMinValue = point.value!
            }
        }
        return globalMinValue
    }
    
    private func findHighestPoints() {
        for row: [Point] in self.map!.points! {
            let globalMaxValue = self.globalMaximumFrom(row)
            
            var considerations: [Point] = [Point]()
            for point: Point in row {
                if point.value == globalMaxValue {
                    considerations.append(point)
                }
            }
            
            // add to list
            self.highestPoints!.appendContentsOf(considerations)
        }
    }
    
    private func cleanHighestPoints() {
        if self.highestPoints == nil || self.highestPoints!.isEmpty {
            return;
        }
        
        let globalMaxValue = self.globalMaximumFrom(self.highestPoints!)
        
        var cleaned: [Point] = [Point]()
        for point: Point in self.highestPoints! {
            if point.value == globalMaxValue {
                cleaned.append(point)
            }
        }
        
        self.highestPoints = cleaned
        self.highestPointValue = globalMaxValue
    }
    
    private func findLowestPoints() {
        for row: [Point] in self.map!.points! {
            let globalMinValue = self.globalMinimumFrom(row)
            
            var considerations: [Point] = [Point]()
            for point: Point in row {
                if point.value == globalMinValue {
                    considerations.append(point)
                }
            }
            
            // add to list
            self.lowestPoints!.appendContentsOf(considerations)
        }
    }
    
    private func cleanLowestPoints() {
        if self.lowestPoints == nil || self.lowestPoints!.isEmpty {
            return;
        }
        
        let globalMinValue = self.globalMinimumFrom(self.lowestPoints!)
        
        var cleaned: [Point] = [Point]()
        for point: Point in self.lowestPoints! {
            if point.value == globalMinValue {
                cleaned.append(point)
            }
        }
        
        self.lowestPoints = cleaned
        self.lowestPointValue = globalMinValue
    }
    
    func doPreAnalysis() {
        if self.map == nil {
            return;
        }
        
        // clear all
        self.highestPoints = [Point]()
        self.lowestPoints = [Point]()
        self.possiblePaths = [PossiblePath]()
        
        // scan for highest and lowest points
        self.findHighestPoints()
        self.cleanHighestPoints()
        
        self.findLowestPoints()
        self.cleanLowestPoints()
    }
    
    private func nextDescendingPoints(currentPoint: Point) -> [Point] {
        var points: [Point] = [Point]()
        var x: Int, y: Int = 0
        
        // north
        x = currentPoint.x!
        y = currentPoint.y! - 1
        if y >= 0 {
            // handle edge
            if self.map!.pointAt(x, y: y)!.value! < currentPoint.value! {
                points.append(self.map!.pointAt(x, y: y)!)
            }
        }
        
        // east
        x = currentPoint.x! + 1
        y = currentPoint.y!
        if x <= self.map!.numberOfColumns!-1 {
            // handle edge
            if self.map!.pointAt(x, y: y)!.value! < currentPoint.value! {
                points.append(self.map!.pointAt(x, y: y)!)
            }
        }
        
        // south
        x = currentPoint.x!
        y = currentPoint.y! + 1
        if y <= self.map!.numberOfRows!-1 {
            // handle edge
            if self.map!.pointAt(x, y: y)!.value! < currentPoint.value! {
                points.append(self.map!.pointAt(x, y: y)!)
            }
        }
        
        // west
        x = currentPoint.x! - 1
        y = currentPoint.y!
        if x >= 0 {
            // handle edge
            if self.map!.pointAt(x, y: y)!.value! < currentPoint.value! {
                points.append(self.map!.pointAt(x, y: y)!)
            }
        }
        
        return points
    }
    
    private func buildPathWith(currentPoint: Point, currentPath: PossiblePath) {
        // add currentPoint to existingPath
        currentPath.add(currentPoint)
        
        let adjacentPoints: [Point]? = self.nextDescendingPoints(currentPoint)
        if adjacentPoints == nil || adjacentPoints!.isEmpty {
            let pathCopy = currentPath.copy() as! PossiblePath
            self.possiblePaths!.append(pathCopy)
            return
        }
        
        // run through each adjacentPoints
        for point: Point in adjacentPoints! {
            self.buildPathWith(point, currentPath: currentPath)
            currentPath.removeLast()
        }
    }
    
    func doPathAnalysis() {
        if self.map == nil {
            return;
        }
        
        // clear all
        self.possiblePaths = [PossiblePath]()
        
        // scan for all possible paths from all possible highest
        for point: Point in self.highestPoints! {
            self.buildPathWith(point, currentPath: PossiblePath())
        }
    }
    
    func bestPath() -> PossiblePath? {
        if self.possiblePaths == nil || self.possiblePaths!.isEmpty {
            return nil
        }
        
        // sort the paths by length value in descending order
        self.possiblePaths?.sortInPlace({ (pathA, pathB) -> Bool in
            return (pathA.length() > pathB.length())
        })
        
        var dropValue: Int = Int.min
        var currentLength: Int = Int.min
        var bestPath: PossiblePath?
        for possiblePath: PossiblePath in self.possiblePaths! {
            if currentLength == Int.min {
                dropValue = possiblePath.drop()
                currentLength = possiblePath.length()
                bestPath = possiblePath
                continue
            }
            
            if possiblePath.length() < currentLength {
                // done. i need longest + steepest!
                break
            }
            
            if possiblePath.drop() > dropValue {
                dropValue = possiblePath.drop()
                bestPath = possiblePath
            }
        }
        
        return bestPath
    }
}